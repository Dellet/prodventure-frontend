$(document).ready(function () {

    // Add listener event for homepage header transition
    $(function() {
        //caches a jQuery object containing the header element
        let header = $("header");
        $(window).scroll(function() {
            let scroll = $(window).scrollTop();

            if (scroll >= 5) {
                header.addClass('scrolled');
            } else {
                header.removeClass('scrolled');
            }
        });
    });



    let humBtn = $('.animated-icon')
    $('.second-button').on('click', function (e) {
        // prevent double click
        // e.preventDefault();
        // let el = $(this);
        // el.prop('disabled', true);
        // setTimeout(function(){el.prop('disabled', false); }, 3000);
        //

        let menuItem = $( e.currentTarget );

        if (menuItem.attr( 'aria-expanded') === 'true' && !humBtn.hasClass('open')) {
            $('li.collapsed-logo').children('a').css("display", "block")
            $('a.navbar-brand').children('img').css("opacity", "0")

            $('body').css("overflow-y", "hidden")
            humBtn.toggleClass('open');

        } else {
            $('li.collapsed-logo').children('a').css("display", "none")
            $('a.navbar-brand').children('img').css("opacity", "1")
            $('body').css("overflow-y", "auto")
            humBtn.removeClass('open');

        }
    });


    let humBtn_2 = $('.animated-icon-kabinet')
    $('.btn-showside').on('click', function (e) {
        if (!humBtn_2.hasClass('open')) {

            humBtn_2.toggleClass('open');

        } else {
            humBtn_2.removeClass('open');

        }
    })
});

let menu_btn = document.querySelector("#menu-btn")
let sidebar = document.querySelector("#sidebar")
let container = document.querySelector(".kabinet-container")
let footer_kabinet = document.querySelector(".footer")
if(menu_btn){
    menu_btn.addEventListener("click", () => {
        sidebar.classList.toggle("active-nav")
        container.classList.toggle("active-cont")
        footer_kabinet.classList.toggle("active-cont")
    })
}

let menu_btn_categories = document.querySelector("#menu-btn-2")
let sidebar_categories = document.querySelector("#sidebar-categories")
let container_categories = document.querySelector(".categories-container")
if(menu_btn_categories){
    menu_btn_categories.addEventListener("click", () => {
        sidebar_categories.classList.toggle("active-nav")
        container_categories.classList.toggle("active-cont")
    })
}

const accord_btns_kabinet = document.querySelectorAll(".accordion-button-kabinet")
const login_modal_contents = document.querySelectorAll(".login-modal-tabs-content")

for (let index = 0; index < accord_btns_kabinet.length; index++) {
    if(accord_btns_kabinet[index]){
        accord_btns_kabinet[index].addEventListener("click", () => {
            login_modal_contents.forEach(el => {
                el.classList.add('d-none')
                el.classList.remove('d-block')
            })
            login_modal_contents[index].classList.remove('d-none')
            login_modal_contents[index].classList.add('d-block')
        })
    }
}


const accord_btns_categories = document.querySelectorAll(".accordion-button-categories")
const categories_modal_contents = document.querySelectorAll(".categories-modal-tabs-content")

for (let index = 0; index < accord_btns_categories.length; index++) {
    if(accord_btns_categories[index]){
        accord_btns_categories[index].addEventListener("click", () => {
            categories_modal_contents.forEach(el => {
                el.classList.add('d-none')
                el.classList.remove('d-block')
            })
            categories_modal_contents[index].classList.remove('d-none')
            categories_modal_contents[index].classList.add('d-block')
        })
    }
}

